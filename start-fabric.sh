#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error
set -e

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
starttime=$(date +%s)

# clean the keystore
rm -rf ./hfc-key-store


export PATH=$PWD/bin:$PATH

# launch network; create channel and join peer to channel
cd ./first-network
echo y | ./byfn.sh down
#echo y | ./byfn.sh up -a -n -i 1.4.8
echo y | ./byfn.sh up -a -n -s couchdb -i 1.4.8

cd ../

# ./deploy_chaincode.sh
./deploy_chaincode_eunomiaTuple.sh
#./deploy_chaincode_eunomiaFull.sh


exit 0

