#!/bin/bash

cd first-network


./byfn.sh down


rm -rf crypto-config/*
rm -rf channel-artifacts/*

docker container rm $(docker container ls -a -q --filter=name="dev-*")
docker image rm $(docker image ls -q --filter=reference="dev-*")
