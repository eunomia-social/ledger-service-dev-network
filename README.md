# Hyperledger single organization sample network 

### Prepare docker images
This step downloads all the required docker images, setup all the required binaries and sets the certificates for the examples
```bash
# Change file mode to executable
chmod +x ./bootstrap.sh
sudo ./bootstrap.sh
# add binary tools to your binary path
export PATH=${PATH}:${PWD}/bin
# make every binary executable
chmod +x bin/*
```
### Chaincode installation
Copy the chaicode under chaincode directory.

Edit deploy-chaincode.sh script and set appropriate values to SRC path and CC_NAME, CC_VERSION.


### Setup organizations and nodes
Make all the scripts executables.

```bash
chmod +x first-network/*.sh
chmod +x first-network/scripts/*.sh
```
Start the first network and deploy the chaincode.

```bash
./start-fabric.sh

```

### Stop network
To stop the network

```bash
./stop_fabric.sh
```
### Network clean up

The ./stop_fabric.sh script does a simple private keys cleanup.
While trying to deploy a new revision of the chaincode without changing the version variable on deploy_chaincode.sh script, you might see that the revised chaincode not running. This happens when there is an already compiled docker container image with the same version as the one you try to compile again.
To clean up such images run the command below:
```bash
docker rmi $(docker image ls -a -q --filter=reference="dev-peer*")
```

### Contributors

Antonios Inglezakis  (@ainglezakis) (email: inglezakis.a@unic.ac.cy)

