#!/bin/bash

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1
starttime=$(date +%s)
CC_RUNTIME_LANGUAGE=golang
#CC_SRC_PATH=github.com/chaincode/
# path for tuple version
CC_SRC_PATH=github.com/chaincode/eunomia-chaincode-go/src/
# path for full version
#CC_SRC_PATH=github.com/chaincode/eunomiachaincode-go/
CC_NAME=eunomia-cc
CC_VERSION=1.0

# clean the keystore
#rm -rf ./hfc-key-store

# launch network; create channel and join peer to channel
#cd ./first-network
#echo y | ./byfn.sh down
#echo y | ./byfn.sh up -a -n 
#echo y | ./byfn.sh up -a -n -s couchdb

CONFIG_ROOT=/opt/gopath/src/github.com/hyperledger/fabric/peer
ORG1_MSPCONFIGPATH=${CONFIG_ROOT}/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
ORG1_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
#ORG2_MSPCONFIGPATH=${CONFIG_ROOT}/crypto/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
#ORG2_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt
ORDERER_TLS_ROOTCERT_FILE=${CONFIG_ROOT}/crypto/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem

set -x 

echo "Submitting queryEntry("POST",1) transaction to smart contract on mychannel"
echo "The transaction is sent to the two peers with the chaincode installed (peer0.org1.example.com ) so that chaincode is built before receiving the following requests"
docker exec \
  -e CORE_PEER_LOCALMSPID=Org1MSP \
  -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
  cli \
  peer chaincode invoke \
    -o orderer.example.com:7050 \
    -C mychannel \
    -n ${CC_NAME} \
    -c '{"function":"queryEntry","Args":["POST","1"]}' \
    --waitForEvent \
    --tls \
    --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
    --peerAddresses peer1.org1.example.com:8051 \
    --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \


echo "Submitting createEntry("POST",123) transaction to smart contract on mychannel"
echo "The transaction is sent to the two peers with the chaincode installed (peer0.org1.example.com ) so that chaincode is built before receiving the following requests"
docker exec \
    -e CORE_PEER_LOCALMSPID=Org1MSP \
    -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
    cli \
    peer chaincode invoke \
        -o orderer.example.com:7050 \
        -C mychannel \
        -n ${CC_NAME} \
        -c '{"function":"createEntry","Args":["POST","123","131111","1NaAxX16TkgQsY6vCVvsyD1tvxWLj6oU5d","GCCV443Y6JZHZNWI36KGB46RL6UC7TMQS3JTXH2CLGMYCMZQFG35NECH"]}' \
        --waitForEvent \
        --tls \
        --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
        --peerAddresses peer0.org1.example.com:7051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \
        --peerAddresses peer1.org1.example.com:8051 \
        --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE}


echo "Submitting queryEntry("POST",123) transaction to smart contract on mychannel"
echo "The transaction is sent to the two peers with the chaincode installed (peer0.org1.example.com ) so that chaincode is built before receiving the following requests"
docker exec \
  -e CORE_PEER_LOCALMSPID=Org1MSP \
  -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
  cli \
  peer chaincode invoke \
    -o orderer.example.com:7050 \
    -C mychannel \
    -n ${CC_NAME} \
    -c '{"function":"queryEntry","Args":["POST","123"]}' \
    --waitForEvent \
    --tls \
    --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
    --peerAddresses peer1.org1.example.com:8051 \
    --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \


echo "Submitting queryEntries([{"POST",1},{"POST",123}]) transaction to smart contract on mychannel"
echo "The transaction is sent to the two peers with the chaincode installed (peer0.org1.example.com ) so that chaincode is built before receiving the following requests"
docker exec \
  -e CORE_PEER_LOCALMSPID=Org1MSP \
  -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
  cli \
  peer chaincode invoke \
    -o orderer.example.com:7050 \
    -C mychannel \
    -n ${CC_NAME} \
    -c '{"function":"queryEntries","Args":["[{\"type\":\"POST\",\"id\":\"1\"},{\"type\":\"POST\",\"id\":\"123\"}]"]}' \
    --waitForEvent \
    --tls \
    --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
    --peerAddresses peer1.org1.example.com:8051 \
    --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \

# //["[{\"type\":\"POST\",\"id\":\"1\"},{\"type\":\"POST\",\"id\":\"123\"}]"]

echo "Submitting queryEntryHistory("POST",1) transaction to smart contract on mychannel"
echo "The transaction is sent to the two peers with the chaincode installed (peer0.org1.example.com ) so that chaincode is built before receiving the following requests"
docker exec \
  -e CORE_PEER_LOCALMSPID=Org1MSP \
  -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
  cli \
  peer chaincode invoke \
    -o orderer.example.com:7050 \
    -C mychannel \
    -n ${CC_NAME} \
    -c '{"function":"queryEntryHistory","Args":["POST","1"]}' \
    --waitForEvent \
    --tls \
    --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
    --peerAddresses peer1.org1.example.com:8051 \
    --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \




echo "Submitting queryTypeEntriesHistory("POST") transaction to smart contract on mychannel"
echo "The transaction is sent to the two peers with the chaincode installed (peer0.org1.example.com ) so that chaincode is built before receiving the following requests"
docker exec \
  -e CORE_PEER_LOCALMSPID=Org1MSP \
  -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
  cli \
  peer chaincode invoke \
    -o orderer.example.com:7050 \
    -C mychannel \
    -n ${CC_NAME} \
    -c '{"function":"queryTypeEntriesHistory","Args":["POST"]}' \
    --waitForEvent \
    --tls \
    --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
    --peerAddresses peer1.org1.example.com:8051 \
    --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \



echo "Submitting deleteEntry("POST",123) transaction to smart contract on mychannel"
echo "The transaction is sent to the two peers with the chaincode installed (peer0.org1.example.com ) so that chaincode is built before receiving the following requests"
docker exec \
  -e CORE_PEER_LOCALMSPID=Org1MSP \
  -e CORE_PEER_MSPCONFIGPATH=${ORG1_MSPCONFIGPATH} \
  cli \
  peer chaincode invoke \
    -o orderer.example.com:7050 \
    -C mychannel \
    -n ${CC_NAME} \
    -c '{"function":"deleteEntry","Args":["POST","123"]}' \
    --waitForEvent \
    --tls \
    --cafile ${ORDERER_TLS_ROOTCERT_FILE} \
    --peerAddresses peer1.org1.example.com:8051 \
    --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \
    --peerAddresses peer0.org1.example.com:7051 \
    --tlsRootCertFiles ${ORG1_TLS_ROOTCERT_FILE} \


set +x